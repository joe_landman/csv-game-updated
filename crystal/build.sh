#!/bin/bash
source ../build.sh
(cd csv && crystal build csv.cr --release --no-debug -o csv)

timer ../results.csv crystal csv fieldcount "./csv/csv < /tmp/hello.csv"
timer ../results.csv crystal csv empty "./csv/csv < /tmp/empty.csv"
