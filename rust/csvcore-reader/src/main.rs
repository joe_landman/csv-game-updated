extern crate csv_core;
extern crate memmap;

use std::env;
use memmap::{Mmap, Protection};

fn main() {
    let fpath = env::args_os().nth(1).unwrap();
    let mmap = match Mmap::open_path(fpath, Protection::Read) {
        Ok(mmap) => mmap,
        Err(_) => { println!("0"); return; }
    };
    let mut data = unsafe { mmap.as_slice() };

    let mut rdr = csv_core::Reader::new();
    let mut count = 0;
    let (mut out, mut end) = ([0; 8192], [0; 100]);
    loop {
        let (res, nin, _, nend) = rdr.read_record(data, &mut out, &mut end);
        data = &data[nin..];
        count += nend;
        match res {
            csv_core::ReadRecordResult::End => break,
            _ => {}
        }
    }
    println!("{}", count);
}
